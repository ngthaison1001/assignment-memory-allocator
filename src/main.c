#include "test.h"
#define HEAP_INIT 10000

int main() {
    // initialize heap and check
    printf("initialize heap:\n");
    void * heap = heap_init(HEAP_INIT);
    if (heap == NULL) err("heap init failed.\n");
    debug_heap(stdout, heap);
    // run tests with initial heap
    if (test1(heap) && test2(heap) && test3(heap) && test4(heap) && test5(heap)) {
        printf("============\nall tests passed.\n");
        return 0;
    }
    return 1;
}