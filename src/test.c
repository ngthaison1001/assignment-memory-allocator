#define _DEFAULT_SOURCE

#include "test.h"
#include "mem.h"

#include <unistd.h>
#include <stdbool.h>

#define BLOCK_MIN_CAPACITY 24
#define QUERY0 16
#define QUERY1 128
#define QUERY2 512

 bool test1(void* heap) {
    printf("test 1: normal memory allocation\n");

    // check address that _malloc returned
    void* mem0 = _malloc(QUERY0);
    void* mem1 = _malloc(QUERY1);

    if (mem0 == NULL || mem1 == NULL) err("allocate NULL.\n");

    // check status and capacity of allocated heap
    struct block_header* header0 = block_get_header(mem0);
    struct block_header* header1 = block_get_header(mem1);

    if (header0->is_free || header0->capacity.bytes != BLOCK_MIN_CAPACITY) {
        printf("_malloc0 failed.\n");
        return false;
    }

    if (header1->is_free || header1->capacity.bytes != QUERY1) {
        printf("_malloc1 failed.\n");
        return false;
    }

    debug_heap(stdout, heap);

    // free
    _free(mem0);
    _free(mem1);

    printf("==> test 1 passed.\n\n");
    return true;
}

 bool test2(void* heap) {
    printf("test 2: free 1 block\n");

    void* mem0 = _malloc(QUERY0);
    void* mem1 = _malloc(QUERY1);
    void* mem2 = _malloc(QUERY2);

    struct block_header* header = block_get_header(mem2);

    printf("before freeing:\n");
    debug_heap(stdout, heap);

    printf("freeing the 3nd block (cap = 512)...\n");
    _free(mem2);

    if (!header->is_free) {
        printf("free failed.\n");
        return false;
    }

    printf("after freeing:\n");
    debug_heap(stdout, heap);

    _free(mem0);
    _free(mem1);

    printf("==> test 2 passed.\n\n");
    return true;
}

 bool test3 (void* heap) {
    printf("test 3: free 2 blocks\n");

    void* mem0 = _malloc(QUERY0);
    void* mem1 = _malloc(QUERY1);
    void* mem2 = _malloc(QUERY2);

    struct block_header* header0 = block_get_header(mem0);
    struct block_header* header1 = block_get_header(mem1);

    printf("before the free:\n");
    debug_heap(stdout, heap);

    printf("freeing the 1st block...\n");
    _free(mem0);
    printf("freeing the 2nd block...\n");
    _free(mem1);

    if (!(header0->is_free && header1->is_free)) {
        printf("free failed.\n");
        return false;
    }

    printf("after the free:\n");
    debug_heap(stdout, heap);

    _free(mem2);

    printf("==> test 3 passed.\n\n");
    return true;
}

 bool test4(void* heap) {
    printf("test 4: memory run out, expand heap\n");

    void* mem1 = _malloc(5000);
    printf("before expanding:\n");
    debug_heap(stdout, heap);

    void* mem2 = _malloc(10000);
    printf("after expanding:\n");
    debug_heap(stdout, heap);

    struct block_header* block1 = block_get_header(mem1);
    struct block_header* block2 = block_get_header(mem2);

    if(block1->next != block2) {
        printf("test failed: expand heap failed.\n");
        return false;
    }

    _free(mem1);
    _free(mem2);

    printf("==> test 4 passed.\n\n");
    return true;
}

 bool test5(void* heap) {
    printf("test 5: memory run out, not expand heap, open new region\n");

    void* mem1 = _malloc(QUERY1);
    void* mem2 = _malloc(QUERY2);

    printf("before, normally allocating:\n");
    debug_heap(stdout, heap);

    struct block_header* last_block = block_get_header(mem2);
    while (last_block->next != NULL)
        last_block = last_block->next;

    void* new_region_addr = (void *) (last_block->contents + last_block->capacity.bytes);
    void *new_region = mmap(new_region_addr, 30000,
                     PROT_READ | PROT_WRITE,
                     MAP_PRIVATE | MAP_ANONYMOUS,
                           0, 0);


    if (new_region == MAP_FAILED) {
        printf("test failed: mapping failed/n");
        return false;
    }

    void* mem3 = _malloc(30000);
    struct block_header* new_block = block_get_header(mem3);

    if (new_block->is_free == true) {
        printf("test failed: new block must be busy./n");
        return false;
    }

    printf("after allocating a block, that bigger than the rest of region:\n");
    debug_heap(stdout, heap);

    _free(mem1);
    _free(mem2);
    _free(mem3);

    printf("==> test 5 passed.\n\n");
    return true;
}
