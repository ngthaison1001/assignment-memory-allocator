#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

// check the capacity of block
static bool block_is_big_enough( size_t query, struct block_header* block ) {
    return block->capacity.bytes >= query;
}
/* getpagesize() returns the number of bytes in a page */
// get the number of pages in memory
static size_t          pages_count   ( size_t mem ) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}
// get the number of bytes in the pages -> lam tron so byte so vs mem
static size_t          round_pages   ( size_t mem ) {
    return getpagesize() * pages_count( mem ) ;
}

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
    *((struct block_header*)addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

// compare size to REGION_MIN_SIZE
static size_t region_actual_size( size_t query ) {
    return size_max( round_pages( query ), REGION_MIN_SIZE );
}

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    /*  ??? */
    struct region alloc_region;
    // get size, not less than REGION_MIN_SIZE
    const size_t actual_size = region_actual_size(query);
    const block_size block_sz = {actual_size};
    // allocate memory at the desired address
    void* region_addr = map_pages(addr, actual_size, MAP_FIXED_NOREPLACE);

    if (region_addr == MAP_FAILED) {
        // if map failed, try to allocate again anywhere
        region_addr = map_pages(addr, actual_size, 0);
        alloc_region = (struct region) {region_addr, actual_size, false};
        // if still failed, return invalid region
        if (region_addr == MAP_FAILED) return REGION_INVALID;
    } else {
        // allocate correctly, assign
        alloc_region = (struct region) {region_addr, actual_size, true};
    }
    // init the first block, next address is null
    block_init(region_addr, block_sz, NULL);
    return alloc_region;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
    const struct region region = alloc_region( HEAP_START, initial );
    if ( region_is_invalid(&region) ) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */
// check whether block can be splitted
static bool block_splittable( struct block_header* restrict block, size_t query) {
    return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    /*  ??? */
    // 1st block: capacity = query, 2nd block: capacity = capacity of block - query
    if (block_splittable(block, query)) {
        void * split_block_addr = (void*)((uint8_t*) block + offsetof(struct block_header, contents) + query);
        const block_size split_block_sz = (block_size) {block->capacity.bytes - query};
        // init new block
        block_init(split_block_addr, split_block_sz, block->next);
        // set up old block
        block->capacity.bytes = query;
        block->next = split_block_addr;
        return true;
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */

// return block lien sau
static void* block_after( struct block_header const* block )              {
    return  (void*) (block->contents + block->capacity.bytes);
}
// check 2 block lien tiep bang block_after
static bool blocks_continuous (
        struct block_header const* fst,
        struct block_header const* snd ) {
    return (void*)snd == block_after(fst);
}
// dieu kien co the merge 2 block
static bool mergeable(  struct block_header const* restrict fst,
                        struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}
// merge this block with the next block
static bool try_merge_with_next( struct block_header* block ) {
    /*  ??? */
    if (block->next && mergeable(block, block->next)) {
        block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
        block->next = block->next->next;
        return true;
    } return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
    struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    /*??? */
    /* neu k nhu minh nghi -> over loop */
    // merge all possible blocks then
    while (block -> next != NULL) {
        while (try_merge_with_next(block));
        // check if the block is big enough
        if (block_is_big_enough(sz, block ) && block->is_free) {
            return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, block};
        }
        block = block->next;
    }
    // use conditional of split_if_too_big
    if (split_if_too_big(block, sz ))
        return (struct block_search_result) { BSR_FOUND_GOOD_BLOCK, block};
    else return (struct block_search_result) { BSR_REACHED_END_NOT_FOUND, block};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    const struct block_search_result result = find_good_or_last(block, query);
    // if good block's found & it's big, split
    if (result.type == BSR_FOUND_GOOD_BLOCK)
        split_if_too_big(result.block, query);
    return result;
}
// extend the heap
static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    /*  ??? */
    struct region new_region;
    // allocate region, get addr through block_after
    if (block_after(last) != NULL) {
        new_region.addr = block_after(last);
        new_region = alloc_region(new_region.addr, query);
    } else
        return NULL;
    // link the last block to the new region
    last->next = new_region.addr;
    if (try_merge_with_next(last))
        return last;
    else return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    /*  ??? */
    const size_t fixed_query = size_max(query, BLOCK_MIN_CAPACITY);
    // find block from heap_start
    struct block_search_result result = try_memalloc_existing(fixed_query, heap_start);
    struct block_header* block = result.block;

    if (result.type == BSR_REACHED_END_NOT_FOUND){
        block = grow_heap(block, fixed_query);
    }
    else if (result.type == BSR_CORRUPTED) {
        block = heap_init(fixed_query);
    }

    // include the case: result.type = BSR_FOUND_GOOD_BLOCK
    split_if_too_big(block, fixed_query);
    block->is_free = false;
    return block;
}

void* _malloc( size_t query ) {
    struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
    if (addr) return addr->contents;
    else return NULL;
}

struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
    if (!mem) return ;
    struct block_header* header = block_get_header( mem );
    header->is_free = true;
    /*  ??? */
    // merge to other free blocks
    while (header != NULL) {
        try_merge_with_next(header);
        header = header->next;
    }
}
